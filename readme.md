# Brown Rabbit

This is the job test if you wish to be a web developer at Twentyfour

## Comments by Shelly
Created a website based on the given interactive web design. Using React JS and Bootstrap for styling.
Deployed on netlify : https://shellyp-brown-rabbit.netlify.app/


- Search functionality implemented on home page. 
Note: you can search and check the section area. it will show only contents with the word world.

- Slider implemented . it changes the image with text on change of the slide.

- Read more button implemented, it shows the rest of the text on click. Show less to back.

- Click on the title of blog on home page will redirect to blog details page.

- one click event: On click of title in home page
  and loop: blog list is populated on home page

- Added Pagination (5 articles on each page)

- Implemented media queries to make it responsive for mobile, tab and desktop (section.css)

- data.js is created with json data for the blogs.


## Contents

When you have completed this test you will have demonstrated skills/knowledge in

- git
- html
- css
- js/jQuery
- Bootstrap

## Requirements

Apply responsivness, make sure that the page look good on the following media queries:

- Mobile - 375px
- Tablet - 768px
- Desktop - 1200px

Search functionality:

- You should be able to search through the content of the site

Slider:

- Create a slider that changes the image and quote text upon sliding.

Read more button:

- Use a click event to show the rest of the text

## Bonus Requirement

Pagination :

- You should be able to navigate through the articles. Make sure you have at least 15 articles.
- Use at least 1 loop and 1 click event

## Instructions

To complete this test.

1. Fork this repository
2. Create a website based on the [interactive webdesign](https://xd.adobe.com/view/e3a1c6ff-52f7-41ed-9e70-bad4e6e9b930-b6f2/grid) (it is not required to use the images in the design)
3. Create a pull request to this repository

## The webdesign

You can find the interactive webdesign here to explore:
[Interactive webdesign](https://xd.adobe.com/view/e3a1c6ff-52f7-41ed-9e70-bad4e6e9b930-b6f2/grid)

The design consists of a Frontpage design and mobile design.
All images and icons can be found in the assets folder including the logo.
You will also find a Styleguide in reference to the font sizes, colours etc.
